using Godot;
using System;

public class HealthBar : ProgressBar
{

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    public void SetColour(Color color) {
        AddColorOverride("font_color", color);
    }

    public void onHealthChanged(float health) {
        Value = health;
    }
}
