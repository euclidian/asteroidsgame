using Godot;
using System;
using System.Collections.Generic;

public class Player : Node2D
{

    [Export]
    public PackedScene MissileAttack;

    [Export]
    public PackedScene SplitMissileAttack;

    [Export]
    public PackedScene TargetingMissileAttack;

    [Export]
    public PackedScene MinesAttack;

    [Export]
    public PackedScene MiningAttack;


    // Signal that the players turn is finished
    [Signal]
    delegate void TurnFinished();

    // The players colour
    [Export]
    public Color Color;

    // The player name
    [Export]
    public String DisplayName;

    // The current site that will be played on the turn
    private int _CurrentSite = 0;
    private List<Site> _OwnedSites = new List<Site>();

    // The inventory of items the player has
    private Dictionary<String, int> _Inventory;

    // Random number gen
    Random _Random;

    // In turn - Avoid repeat calls
    Boolean _InTurn = false;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // Duplicate the health box style so others can alter
        SetColour(Color);
        SetDisplayName(DisplayName);
        UpdateScore();
        ResetInventory();
    }

    public void SetSitesPerPlayer(int number_sites) {
        ProgressBar health = GetNode<ProgressBar>("UILayer/Panel/MarginContainer/VBoxContainer/Health");
        health.MaxValue = 100 * number_sites;
    }

    public void SetColour(Color colour) {
        Color = colour;
        ProgressBar health = GetNode<ProgressBar>("UILayer/Panel/MarginContainer/VBoxContainer/Health");
        var new_style = (StyleBoxFlat)health.GetStylebox("fg").Duplicate();
        new_style.SetBgColor(Color);
        health.AddStyleboxOverride("fg", new_style);
        _Random = new Random();
        GetNode<CanvasLayer>("UILayer").SetOffset(GlobalPosition);
        Label name = GetNode<Label>("UILayer/Panel/MarginContainer/VBoxContainer/Name");
        Label score = GetNode<Label>("UILayer/Panel/MarginContainer/VBoxContainer/CenterContainer/Score");
        name.AddColorOverride("font_color", Color);
        score.AddColorOverride("font_color", Color);
    }

    public void SetDisplayName(String display_name) {
        DisplayName = display_name;
        Label name = GetNode<Label>("UILayer/Panel/MarginContainer/VBoxContainer/Name");
        name.Text = DisplayName;
    }

    // Start the players turn
    //
    // Start the turn of the current site
    public void StartTurn() {
        // Set up the inventory with the dictionary
        GetInventoryUI().Show();
        GetInventoryUI().SetInventory(_Inventory);
        _InTurn = true;
        if(IsOut()) {
            // No sites skip.
            EmitSignal("TurnFinished");
            return;
        }
        // Get the next site to start
        _CurrentSite++;
        if(_CurrentSite >= _OwnedSites.Count) {
            _CurrentSite = 0;
        }
        _OwnedSites[_CurrentSite].StartTurn();
    }

    // Called when the players attack is finshed
    //
    // Signal the turn is over
    public void onAttackFinished() {
        if(_InTurn) {
            EmitSignal("TurnFinished");
            _InTurn = false;
        }
    }

    // Add a site to the ones this player owns
    public void AddSite(Site site) {
        _OwnedSites.Add(site);
        UpdateScore();
    }

    // Out if we own no sites
    public bool IsOut() {
        return _OwnedSites.Count == 0;
    }

    // Remove a site to the ones this player owns
    public void RemoveSite(Site site) {
        _OwnedSites.Remove(site);
        UpdateScore();
    }

    // A site the player owns has been destroyed
    public void SiteDestroyed(Site site) {
        _OwnedSites.Remove(site);
        if(_CurrentSite >= _OwnedSites.Count) {
            _CurrentSite = 0;
        }
        UpdateScore();
    }

    // Start the attack
    //
    // This will be called with all the different attacks when one is launched.
    //
    // The player who launched the attach the global coordinates of the attack, it's direction and 0-1 magnitude.
    public void LaunchAttack(
        Site site,
        Vector2 location,
        Vector2 direction,
        float magnitude
    ) {
        var attack_name = GetInventoryUI().SelectedAttack();
        // Decrement the inventory 
        if(_Inventory[attack_name] > 0) {
            _Inventory[attack_name]--;
        }
        GetInventoryUI().Hide();
        // Will choose an attack based on selection for now just fire missile
        var attackInstance = CreateAttack(attack_name);
        AddChild(attackInstance);
        attackInstance.LaunchAttack(site, location, direction, magnitude);
    }

    private InventoryPanel GetInventoryUI(){
        return GetNode<InventoryPanel>("/root/Main/UILayer/InventoryUI");
    }

    // Create an instance of the attack we will next launch
    private Attack CreateAttack(String attack_name) {
        if(attack_name == "missile_multi") {
            return (Attack)SplitMissileAttack.Instance();
        }
        if(attack_name == "missile_target") {
            return (Attack)TargetingMissileAttack.Instance();
        }
        if(attack_name == "mines") {
            return (Attack)MinesAttack.Instance();
        }

        if(attack_name == "asteroid_mine") {
            return (Attack)MiningAttack.Instance();
        }
        // Don't know which one default to missile
        return (Attack)MissileAttack.Instance();
    }

    public void RandomInventoryAdd() {
        // Choose a slot - the first and last are infinite so exclude.
        var slot = _Random.Next(1, 6);
        // Chose amount
        var amount = _Random.Next(1, 3);
        var order = InventoryOrder();
        _Inventory[order[slot]] += amount;
    }

    public override void _Process(float delta) {
        UpdateScore();
    }

    private void UpdateScore() {
        Label score = GetNode<Label>("UILayer/Panel/MarginContainer/VBoxContainer/CenterContainer/Score");
        score.Text = _OwnedSites.Count.ToString();
        ProgressBar health = GetNode<ProgressBar>("UILayer/Panel/MarginContainer/VBoxContainer/Health");
        health.Value = GetHealth();
    }

    private float GetHealth() {
        var health = 0.0f;
        foreach(var site in _OwnedSites) {
            health += 100.0f * site.GetHealth() / site.MaxHealth;
        }
        return health;
    }

    private void ResetInventory() {
        _Inventory = new Dictionary<string, int>();
        _Inventory["missile"] = -1;
        _Inventory["missile_multi"] = 3;
        _Inventory["missile_target"] = 2;
        _Inventory["mines"] = 2;
        _Inventory["asteroid_yank"] = 0;
        _Inventory["black_hole"] = 0;
        _Inventory["white_hole"] = 0;
        _Inventory["asteroid_mine"] = -1;
    }

    private List<String> InventoryOrder() {
        var order = new List<String>();
        order.Add("missile");
        order.Add("missile_multi");
        order.Add("missile_target");
        order.Add("mines");
        order.Add("asteroid_yank");
        order.Add("black_hole");
        order.Add("white_hole");
        order.Add("asteroid_mine");
        return order;
    }
}
