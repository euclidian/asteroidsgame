using Godot;
using System;

public class TargetingMissile : Missile
{
    // The inital site we ignore
    public Player PlayerOwner;

    // Aim towards Each of the enermy sites
    public override Vector2 GetGravity() {
        var gravity = GetNode<Main>("/root/Main").GetGravityEffect(GlobalPosition);
        foreach(var site in GetNode<Main>("/root/Main").GetSites()) {
            if(site.SameOwner(PlayerOwner)) {
                gravity -= site.TargetingEffect(GlobalPosition) * 0.1f;
            } else {
                gravity += site.TargetingEffect(GlobalPosition);
            }
        }
        return gravity;
    }
}
