using Godot;
using System;

public class WinScreen : Control
{
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GetNode<Timer>("Timer").Start();
        var player_vars = GetNode<PlayerVars>("/root/PlayerVars");
        GetNode<Label>("PlayerName").Text = player_vars.Victor.Name;
        GetNode<ColorRect>("ColorRect").Color = player_vars.Victor.Colour;
    }

    public void Finished() 
    {
        GetTree().ChangeScene("res://StartMenu.tscn");
    }
}
