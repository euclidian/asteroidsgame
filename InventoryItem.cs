using Godot;
using System;

public class InventoryItem : Control
{
    [Export]
    public String InventoryItemName;
    [Export]
    public String InventoryTooltip;

    private int _InventoryNumber = 0;

    // Are we selected
    private Boolean _Selected = false;


    // Signal to inventory this item has been selected
    [Signal]
    delegate void Selected(InventoryItem item);

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // Set up the name and load hint
        LoadItemImage();
        SetNumber(0);
        // Duplicate the style so differnt buttons can alter
        var panel = GetNode<PanelContainer>("MarginContainer/Panel");
        panel.AddStyleboxOverride(
            "panel", 
            (StyleBox)panel.GetStylebox("panel").Duplicate()
        );
        unSelect();
    }

    private void LoadItemImage() {
        // All the item icons are saved as name_icon_full
        String image_path = String.Format("res://images/{0}_icon_full.png", InventoryItemName);
        Texture image = (Texture)GD.Load(image_path);
        TextureButton icon = GetNode<TextureButton>("MarginContainer/Panel/Button");
        icon.TextureNormal = image;
        icon.SetTooltip(InventoryTooltip);
    }

    private void LoadNoItemImage() {
        // All the item icons are saved as name_icon_full
        String image_path = String.Format("res://images/no_item_icon_full.png", InventoryItemName);
        Texture image = (Texture)GD.Load(image_path);
        TextureButton icon = GetNode<TextureButton>("MarginContainer/Panel/Button");
        icon.TextureNormal = image;
        icon.SetTooltip("Not enough resources to use this!");
    }

    // Set the number of items in the inventory
    public void SetNumber(int number) {
        _InventoryNumber = number;
        var number_label = GetNode<Label>("Number");
        // If we don't have any of the item diesplay no image else display it's image
        if(number==0) {
            LoadNoItemImage();
            number_label.Hide();
        } else {
            LoadItemImage();
            // Update the label - the number -1 signifies infinite
            number_label.Show();
            if(number == -1) {
                number_label.Text = ""; // Ideally have an infinity symbol
            }else {
                number_label.Text = number.ToString();
            }
        }
    }

    public int NumberItems() {
        return _InventoryNumber;
    }

    // Unselect the node
    public void unSelect() {
        _Selected = false;
        var panel = GetNode<PanelContainer>("MarginContainer/Panel");
        var style = (StyleBoxFlat)panel.GetStylebox("panel");
        style.BorderColor = new Color(0.309804f, 0.27451f, 0.27451f, 1);
    }

    // When mouse goes over
    public void onEnter() {
        // If we arent selected and have items highlight
        if(!_Selected && _InventoryNumber!=0) {
            var panel = GetNode<PanelContainer>("MarginContainer/Panel");
            var style = (StyleBoxFlat)panel.GetStylebox("panel");
            style.BorderColor = new Color(0.5f, 0.5f, 0.5f, 1);
        }
    }

    // When mouse goes over
    public void onLeave() {
        // If we arent selected dim
        if(!_Selected && _InventoryNumber!=0) {
            var panel = GetNode<PanelContainer>("MarginContainer/Panel");
            var style = (StyleBoxFlat)panel.GetStylebox("panel");
            style.BorderColor = new Color(0.309804f, 0.27451f, 0.27451f, 1);
        }
    }

    // The button has been pressed so this node is selected
    private void onSelected() {
        // Can only select if we have items
        if(_InventoryNumber!=0){
            _Selected = true;
            // Update drawing
            var panel = GetNode<PanelContainer>("MarginContainer/Panel");
            var style = (StyleBoxFlat)panel.GetStylebox("panel");
            style.BorderColor = new Color(0.6f, 0.6f, 0.6f, 1);
            // Tell the inventory
            EmitSignal("Selected", this);
        }
    }
}
