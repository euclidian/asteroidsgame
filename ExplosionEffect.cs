using Godot;
using System;

public class ExplosionEffect : Node2D
{

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // Start the timer
        GetNode<Timer>("Lifetime").Start();
        // Start Explosion
        GetNode<Particles2D>("Particles2D").Restart();
    }

    public void onLifetimeEnd()
    {
        // Remove
        QueueFree();
    }

}
