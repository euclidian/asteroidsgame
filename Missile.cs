using Godot;
using System;

public class Missile : RigidBody2D
{

    // Signal that the missile has run out of time
    [Signal]
    delegate void Timeout();

    // Signal a collision with another entity causing destruction
    [Signal]
    delegate void CollideExplosion(Vector2 position);

    // The lifetime of the missile
    [Export]
    public float Lifetime = 9.0f;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Timer lifetime = GetNode<Timer>("Lifetime");
        lifetime.SetWaitTime(Lifetime);
        lifetime.Start();
        GetNode<Main>("/root/Main").TargetView(this);
    }

    public override void _Process(float delta) {
        // Get the sprite/bound box pointing in the direction in of the velocity.
        var bodyForward = (new Vector2(0, -1)).Rotated(GlobalRotation);
        var angular_diff = -GetLinearVelocity().AngleTo(bodyForward);
        GetNode<Sprite>("Sprite").Rotation = angular_diff;
        GetNode<CollisionShape2D>("CollisionShape2D").Rotation = angular_diff;
    }

    public override void _PhysicsProcess(float delta) {
        Vector2 gravity = GetGravity();
        SetAppliedForce(gravity);
    }

    public virtual Vector2 GetGravity() {
        return GetNode<Main>("/root/Main").GetGravityEffect(GlobalPosition);
    }

    // Signal that we have exploded and queue for deletion
    public void onCollideExplosion() {
        EmitSignal("CollideExplosion", this, GlobalPosition);
        GetNode<Main>("/root/Main").RemoveIfTarget(this);
        QueueFree();
    }

    // Signal that we have timedout and queue for deletion
    public void LifetimeTimeout() {
        EmitSignal("Timeout", this);
        GetNode<Main>("/root/Main").RemoveIfTarget(this);
        QueueFree();
    }

}
