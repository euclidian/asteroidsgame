using Godot;
using System;
using System.Collections.Generic;

public class PlayerMenu : Control
{

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        var player_vars = GetNode<PlayerVars>("/root/PlayerVars");
        if(player_vars.Players != null) {
            var player_path = "HBoxContainer/MarginContainer/VBoxContainer/PlayerEntry";
            for(int i = 1; i < 5; ++i) {
                var player_node = player_path + i.ToString();
                var entry = GetNode<PlayerEntry>(player_node);
                var player_var = player_vars.Players[i];
                entry.SetPlayerName(player_var.Name);
                entry.SetPlaying(player_var.Playing);
                entry.SetColour(player_var.Colour);
            }
        }
    }

    public void onPlay() {
        // Put all the data in global
        var player_vars = GetNode<PlayerVars>("/root/PlayerVars");
        player_vars.Players = new List<PlayerVar>();
        var player_path = "HBoxContainer/MarginContainer/VBoxContainer/PlayerEntry";
        for(int i = 1; i < 5; ++i) {
            var player_node = player_path + i.ToString();
            var entry = GetNode<PlayerEntry>(player_node);
            var player_var = new PlayerVar();
            player_var.Name = entry.GetPlayerName();
            player_var.Playing = entry.GetPlaying();
            player_var.Colour = entry.GetColour();
            player_vars.Players.Add(player_var);
        }

        GetTree().ChangeScene("res://Main.tscn");
    } 

}
