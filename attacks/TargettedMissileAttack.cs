using Godot;
using System;

public class TargettedMissileAttack : MissileAttack
{

    [Export]
    public PackedScene TargetingMissile;

    // Create a targeted missile
    public override RigidBody2D CreateMissile() {
        var missile = (TargetingMissile)TargetingMissile.Instance();
        missile.PlayerOwner = Attacker;
        return missile;
    }

}
