using Godot;
using System;
using System.Collections.Generic;

public class SplitMissileAttack : MissileAttack
{

    // Missile in the second wave. If null we have no second wave
    private List<Missile> _SecondWave;

    // Create missile with shorter timeout
    public override RigidBody2D CreateMissile() {
        var missile = (Missile)base.CreateMissile();
        missile.Lifetime = 1.2f;
        if(_SecondWave != null) {
            // Second wave missiles are smaller and last longer
            missile.Scale = new Vector2(0.7f, 0.7f);
            missile.Lifetime = 8.0f;
            _SecondWave.Add(missile); // Track missile
        }
        return missile;
    }

    public override void MissileTimeout(Missile missile) {
        Console.Out.WriteLine("Timeout");
        if(_SecondWave == null) {
            // First wave finished 
            LaunchSecondWaveAttack(missile);
        } else {
            SecondWaveMissileDestroyed(missile);
        }
    }

    public override void MissileHit(Missile missile, Vector2 position) {
        // Hit something cause bang telling the main about it.
        GetNode<Main>("/root/Main").Explosion(position);

        if(_SecondWave == null) {
            // No second wave
            EndAttack();
        } else {
            // A missile has been destroyed see if this ends the attack
            SecondWaveMissileDestroyed(missile);
        }
    }

    private void SecondWaveMissileDestroyed(Missile missile) {
        if(_SecondWave.Count == 1) {
            // Last missile lost end attack
            EndAttack();
        } else {
            // Remove from second wave
            _SecondWave.Remove(missile);
        }
    }

    private void LaunchSecondWaveAttack(Missile missile) {
        _SecondWave = new List<Missile>();
        // Get the directions and launch 3 missiles.
        Vector2 position = missile.GlobalPosition;
        Vector2 direction = missile.LinearVelocity.Normalized();
        // We scale the magnitude by 250 to get velocity at launch
        float magnitude  = missile.LinearVelocity.Length() / 250.0f;

        Vector2 direction_left = direction.Rotated(-0.48f);
        Vector2 direction_right = direction.Rotated(0.48f);

        Vector2 offset = direction * 5;
        Vector2 offset_left = direction_left * 5;
        Vector2 offset_right = direction_right * 5;
        LaunchAttack(null, position + offset, direction, magnitude);
        LaunchAttack(null, position + offset_left, direction_left, magnitude);
        LaunchAttack(null, position + offset_right, direction_right, magnitude);
    }
}
