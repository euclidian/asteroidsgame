using Godot;
using System;

public class MissileAttack : Attack
{

    [Export]
    public PackedScene Missile;

    // Start the attack
    //
    // This will be called with all the different attacks when one is launched.
    //
    // The player who launched the attach the global coordinates of the attack, it's direction and 0-1 magnitude.
    public override void LaunchAttack(
        Site site,
        Vector2 location,
        Vector2 direction,
        float magnitude
    )
    {
        // Let base class do it's thing
        base.LaunchAttack(site, location, direction, magnitude);

        // Fire a missile
        var missileInstance = CreateMissile();
        AddChild(missileInstance);

        missileInstance.GlobalPosition = location;

        // Choose the velocity.
        var inital_velocity = direction * magnitude * 250f;
        missileInstance.SetLinearVelocity(inital_velocity);

        // Handle the missile timing out or exploding
        missileInstance.Connect("Timeout", this, "MissileTimeout");
        missileInstance.Connect("CollideExplosion", this, "MissileHit");
    }

    public virtual RigidBody2D CreateMissile() {
        return (RigidBody2D)Missile.Instance();
    }

    // Timedout end attack
    public virtual void MissileTimeout(Missile missile) {
        EmitSignal("AttackFinished");
        QueueFree();
    }

    public virtual void MissileHit(Missile missile, Vector2 position) {
        // Hit something cause bang telling the main about it.
        GetNode<Main>("/root/Main").Explosion(position);
        EndAttack();
    }

}
