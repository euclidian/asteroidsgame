using Godot;
using System;

public class Attack : Node2D
{
    // Signal that this missiles attack has finished
    [Signal]
    delegate void AttackFinished();

    // The player who launched the attack
    public Player Attacker;

    // First call - we want to avoid setting up data is we launch repeatedly
    private Boolean _FirstCall = true;

    // Start the attack
    //
    // This will be called with all the different attacks when one is launched.
    //
    // The player who launched the attach the global coordinates of the attack, it's direction and 0-1 magnitude.
    public virtual void LaunchAttack(
        Site site,
        Vector2 location,
        Vector2 direction,
        float magnitude
    )
    {
        // We can launch sub/repeat attacks we dont want to reset the timer.
        if(_FirstCall) {
            Attacker = site.PlayerOwner();
            // Make sure no attack can overrun
            GetNode<Timer>("TurnOver").Start();
            // Our owner needs to know when this attack has finished
            Connect("AttackFinished", Attacker, "onAttackFinished");
        }
        _FirstCall = false;
    }

    // If the overall attack timesout attack is finished
    public void TurnOverTimeout() {
        EndAttack();
    }

    // End the attack
    public void EndAttack() {
        // End attack
        EmitSignal("AttackFinished");
        QueueFree();
    }
}
