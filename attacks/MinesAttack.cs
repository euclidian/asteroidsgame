using Godot;
using System;

public class MinesAttack : Attack
{
    [Export]
    public PackedScene Mine;

    // Start the attack
    //
    // This will be called with all the different attacks when one is launched.
    //
    // The player who launched the attach the global coordinates of the attack, it's direction and 0-1 magnitude.
    public override void LaunchAttack(
        Site site,
        Vector2 location,
        Vector2 direction,
        float magnitude
    )
    {
        // Let base class do it's thing
        base.LaunchAttack(site, location, direction, magnitude);

        for(float angle = -1.1f; angle <=1.1f; angle = angle + 0.22f) {
            var new_direction = direction.Rotated(angle);
            LaunchMine(site, location, new_direction, magnitude);
        }


        // The main attack will timeout out turn.

    }

    private void LaunchMine(
        Site site,
        Vector2 location,
        Vector2 direction,
        float magnitude
    ) {
        // Launch the mines
            var mineInstance = CreateMine();
            // These will be owned by the player not the attack as the attack ends.
            site.PlayerOwner().AddChild(mineInstance);

            // Offset the initial position so they arent all at the same location
            mineInstance.GlobalPosition = location + (direction*6f);

            // Choose the velocity.
            var inital_velocity = direction * magnitude * 250f;
            mineInstance.SetLinearVelocity(inital_velocity);
    }

    public virtual RigidBody2D CreateMine() {
        return (RigidBody2D)Mine.Instance();
    }
}
