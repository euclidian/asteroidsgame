using Godot;
using System;

public class MiningAttack : Attack
{
    private Asteroid _Asteroid;

    // The amount of damage mining does to an asteroid.
    [Export]
    public float Damage = 30;

    // Start the attack
    //
    // This will be called with all the different attacks when one is launched.
    //
    // The player who launched the attach the global coordinates of the attack, it's direction and 0-1 magnitude.
    public override void LaunchAttack(
        Site site,
        Vector2 location,
        Vector2 direction,
        float magnitude
    )
    {
         // Let base class do it's thing
        base.LaunchAttack(site, location, direction, magnitude);

        // Move to the site
        Rotation = site.Rotation;
        GlobalPosition = site.GetNode<Position2D>("BasePosition").GlobalPosition;

        // Set the asteroid to attack
        _Asteroid = GetNode<Main>("/root/Main").GetAsteroidForSite(site);    

        // Set off effects
        GetNode<Particles2D>("Particles2D").Emitting = true; 

        // Give the player goodies
        site.PlayerOwner().RandomInventoryAdd();

        // The main attack will timeout out turn.
    }

    public override void _Process(float delta) {
        var DamageRate = Damage / GetNode<Timer>("TurnOver").WaitTime;
        // Damage the asteroid
        GetNode<Main>("/root/Main").DamageAsteroid(_Asteroid, delta * DamageRate);
    }
}
