using Godot;
using System;

public class Site : Area2D
{

    // Is it the current node's turn to fire
    private bool _FiringTurn = false;

    // The player who owns this site
    private Player _Owner;

    // Members to control where we are targeting
    private Vector2 _TargetDirection; // Normalized direction to fire
    private float _Magnitude = 0; // Force to fire with - from 0 to 1

    [Export]
    // How long before we reach maximum magnitude and clamp in seconds
    public float TimeToMaximumMagnitude = 2;

    [Export]
    // Length of the line to display firing information
    public float FiringDisplayLength = 80;

    [Export]
    // The initial health of the site
    public float MaxHealth = 80;

    // The current health of the site;
    private float _Health;

    // If we are targeting
    private Boolean _Targeting;

    [Signal]
    // Site's health has changed
    delegate void SitesHealthChanged(float health);

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GetNode<AnimatedSprite>("MainBase").Animation = "base";
        var healthBar = GetNode<ProgressBar>("GUI/GUIFrame/HealthBar");
        healthBar.MaxValue = MaxHealth;
        healthBar.Value = MaxHealth;
        _Health = MaxHealth;
    }

    // Check if we should be doing anything
    public override void _Process(float delta)
    {
        PositionGUI();
        if(_Targeting) {
            UpdateTargeting(delta);
        }
    }

    // The GUI positioned at our location. It isn't a direct child as this
    // would add a rotation
    private void PositionGUI() {
        GetNode<Node2D>("GUI/GUIFrame").Position = GlobalPosition;
    }

    public float GetHealth() {
        return _Health;
    }

    public void SetOwner(Player owner) {
        if(_Owner != null) {
            _Owner.RemoveSite(this);
        }
        _Owner = owner;
        _Owner.AddSite(this);
        // Update the colour of children
        GetNode<HealthBar>("GUI/GUIFrame/HealthBar").SetColour(_Owner.Color);
    }

    public Boolean SameOwner(Player owner) {
        return _Owner == owner;
    }

    public Player PlayerOwner() {
        return _Owner;
    }

    // Called when this node starts it's turn to fire
    public void StartTurn() {
        _FiringTurn = true;
        GetNode<AnimatedSprite>("MainBase").Animation = "highlighted";
        // Setup the firing parameters
        ResetTargeting();
        GetNode<Main>("/root/Main").TargetView(this);
    }

    // Called when this node ends it's turn to fire
    private void EndTurn() {
        _FiringTurn = false;
        GetNode<Line2D>("FiringDisplay").Hide();
        GetNode<AnimatedSprite>("MainBase").Animation = "base";
        GetNode<Main>("/root/Main").RemoveIfTarget(this);
    }
    
    private void Fire()
    {
        var launchSite = GetNode<Position2D>("LaunchPosition");
        var position = launchSite.GlobalPosition;
        var direction = _TargetDirection.Rotated(GlobalRotation);
        // Tell our player to launch the attack
        _Owner.LaunchAttack(this, position, direction, _Magnitude);
        // We have fired not our turn any more
        EndTurn();
    }

    // Return targeting to default state
    private void ResetTargeting() {
        _Magnitude = 0;
        _TargetDirection = new Vector2(0,-1);
    }

    // Update targeting increase magnitude and point direction at cursor
    private void UpdateTargeting(float delta) {
        _Magnitude += (delta / TimeToMaximumMagnitude);
        if(_Magnitude > 1) {
            _Magnitude = 1;
        }
        var launchSite = GetNode<Position2D>("LaunchPosition");
        var direction = GetLocalMousePosition() - launchSite.Position;
        // only launch 'forward (negative y in local coords)
        if(direction.y > 0) {
            direction.y = 0;
        }
        // If the cursor is on the launch site don't update
        if(direction.Length() > 0.000001) {
            _TargetDirection = direction.Normalized();
        }
        // Update the targeting display
        var firingDisplay = GetNode<Line2D>("FiringDisplay");
        firingDisplay.Show();
        var offset = _TargetDirection * _Magnitude * FiringDisplayLength;
        firingDisplay.SetPointPosition(1, launchSite.Position + offset);
    }

    // Take area damage - resend to child nodes
    private void onAreaDamage(Vector2 position, float area, float damage) {
        var size = 20;
        if(GlobalPosition.DistanceTo(position) < (area+size)) {
            TakeDamage(damage);
        }
    }

    // The site has taken damage update health
    private void TakeDamage(float damage) {
        _Health -= damage;
        if(_Health < 0) {
            Destroy();
        } else {
            EmitSignal("SitesHealthChanged", _Health);
        }
    }

    // The site has been destroyed
    private void Destroy() {
        // Tell our player
        _Owner.SiteDestroyed(this);
        // Make sure we dont target
        GetNode<Main>("/root/Main").RemoveIfTarget(this);
        QueueFree();
    }

    // The asteroid we are on has been destroyed
    private void onAsteroidDestroyed(Asteroid asteroid) {
        // Destroy ourselves
        Destroy();
    }

    // If something enteres our area iff it is a missile tell it to explode
    private void _on_Site_body_entered(PhysicsBody2D body) {
        if(body is Missile) {
            var missile = (Missile)body;
            missile.onCollideExplosion();
        }
        if(body is Mine) {
            var mine = (Mine)body;
            mine.onCollideExplosion();
        }
    }


    public override void _UnhandledInput(InputEvent @event)
    {
        if (@event is InputEventMouse eventMouse) {
            if(_FiringTurn) {
                // Our turn to fire see if we are holding the mouse down if so update targeting
                if(eventMouse.IsActionPressed("target_click")) {
                    StartTargeting();
                }
                // If we have released the mouse fire
                if(eventMouse.IsActionReleased("target_click")) {
                    StopTargeting();
                    Fire();
                }
            }
        }
    }

    // Say we are going into targeting mode and draw the GUI
    private void StartTargeting() {
        _Targeting = true;
    }

    // Say we are going into targeting mode and draw the GUI
    private void StopTargeting() {
        _Targeting = false;
    }

    // The effects on a missile targetting this site.
    public Vector2 TargetingEffect(Vector2 position) {
        var basicForce = 1 / GlobalPosition.DistanceSquaredTo(position);
        var force = basicForce * 20 * 100 * 100;
        Vector2 delta = (GlobalPosition - position).Normalized();
        return delta * force;
    }

}
