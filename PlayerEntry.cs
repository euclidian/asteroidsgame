using Godot;
using System;

public class PlayerEntry : PanelContainer
{
    [Export]
    public Color Colour;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GetNode<ColorPickerButton>("HBoxContainer/Colour").Color = Colour;
        GetNode<ColorPickerButton>("HBoxContainer/Colour").SetPickColor(Colour);
    }

    public String GetPlayerName() {
        return GetNode<LineEdit>("HBoxContainer/Name").Text;
    }

    public Color GetColour() {
       return GetNode<ColorPickerButton>("HBoxContainer/Colour").Color;
    }

    public Boolean GetPlaying() {
        return GetNode<CheckBox>("HBoxContainer/Playing").Pressed;
    }

    public void SetPlayerName(String name) {
        GetNode<LineEdit>("HBoxContainer/Name").Text = name;
    }

    public void SetColour(Color colour) {
        GetNode<ColorPickerButton>("HBoxContainer/Colour").Color = colour;
    }

    public void SetPlaying(Boolean playing) {
        GetNode<CheckBox>("HBoxContainer/Playing").Pressed = playing;
    }
}
