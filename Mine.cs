using Godot;
using System;

public class Mine : RigidBody2D
{

    [Export]
    public float SpeedHalflife = 1;

    private Boolean _Active;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // Start inactive wait for activation
        GetNode<Timer>("Activation").Start();
        _Active = false;
    }


    // NB factor out some of this common code
    public override void _PhysicsProcess(float delta) {
        if(_Active) {
            SetLinearVelocity(new Vector2(0,0));
            return;
        }
        Vector2 gravity = GetGravity();
        SetAppliedForce(gravity);
        // Slow the asteroids down over time
        float scale = 1.0f / (Mathf.Pow(2, delta / SpeedHalflife));
        SetLinearVelocity(GetLinearVelocity() * scale);
    }

    public virtual Vector2 GetGravity() {
        return GetNode<Main>("/root/Main").GetGravityEffect(GlobalPosition);
    }

    public void Activation() {
        // We are active stop moving and turn active
        _Active = true;
        GetNode<AnimatedSprite>("MineSprite").Animation = "active";
    }

    // When we collide with something else 
    public void onCollideExplosion() {
        GetNode<Main>("/root/Main").MineExplosion(GlobalPosition);
        QueueFree();
    }

    // Is we detect a missile delete it and us
    public void ItemDetected(PhysicsBody2D body) {
        if(_Active) {
            if(body is Missile) {
                var missile = (Missile)body;
                missile.onCollideExplosion();
                onCollideExplosion();
            }
        }
    }
}
