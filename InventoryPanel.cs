using Godot;
using System;
using System.Collections.Generic;

public class InventoryPanel : Control
{

    // Map of attack names to items
    private Dictionary<String, InventoryItem> _ItemMap;

    // The selected item
    private InventoryItem _Selected;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _ItemMap = new Dictionary<String, InventoryItem>();
        foreach(var item in GetItems()) {
            var name = item.InventoryItemName;
            _ItemMap.Add(name, item);
        }
        // Set missile as default selected
        _Selected = _ItemMap["missile"];
    }

    // Get the attack of the selected item
    public String SelectedAttack() {
        return _Selected.InventoryItemName;
    }

    // All the items
    private List<InventoryItem> GetItems() {
        var items = new List<InventoryItem>();
        // All the items are children of the container
        Godot.Collections.Array item_array = GetNode<Node>("PanelContainer/HBoxContainer").GetChildren();
        foreach(var item in item_array) {
            items.Add((InventoryItem)item);
        }
        return items;
    }

    // Call back if an item is selected 
    //
    // Only one can be selected at a time
    public void ItemSelected(InventoryItem item) {
        // Deselect all others
        foreach(var other_item in GetItems()) {
            if(item != other_item) {
                other_item.unSelect();
            }
        }
        // Register as selected
        _Selected = item;
    }

    // Set the inventory
    public void SetInventory(Dictionary<String, int> inventory) {
        foreach(var item in inventory) {
            _ItemMap[item.Key].SetNumber(item.Value);
        }
        // Reset the selection to an item which is always valid
        ItemSelected(_ItemMap["missile"]);
    }
}
