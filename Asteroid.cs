using Godot;
using System;
using System.Collections.Generic;

public class Asteroid : Area2D
{

    [Export]
    public float InitialRadius;

    [Export]
    public float MinimumRadius;

    [Export]
    public PackedScene Site;

    // The radius of the asteroid
    private float _Radius;

    // The collision shape
    private CircleShape2D _CollisionShape;

    // The sprite
    private Sprite _Sprite;

    // Signal for everything within range to take area damage
    [Signal]
    delegate void AreaDamage(Vector2 position, float area, float damage);

    // Signal theat the asteroid has been destroyed
    [Signal]
    delegate void Destroyed(Asteroid asteroid);

    // Signal that the asteroids radius has changed
    [Signal]
    delegate void RadiusChanged();

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _Radius = InitialRadius;

        var collision_object = GetNode<CollisionShape2D>("CollisionShape2D");
        _CollisionShape = (CircleShape2D)collision_object.Shape;
        _Sprite = GetNode<Sprite>("Sprite");
        // Get all the child sites
        foreach(var site in GetSites()) {
            // Set up signal handling for sites
            Connect("AreaDamage", site, "onAreaDamage");
            Connect("Destroyed", site, "onAsteroidDestroyed");
        }
        // Propagate radius change
        EmitSignal("RadiusChanged");
    }

    public void SetRadius(float radius) {
        _Radius = radius;
        // Propagate radius change
        EmitSignal("RadiusChanged");
    }

    public Boolean AddSite(Player player, Random random, Boolean force) {
        // Choose random vector
        var degrees = random.Next(0, 360);
        var radians = (degrees * Mathf.Pi) / 180.0f;
        var up = new Vector2(1, 0);
        var distance = _Radius + 10;
        var position = up.Rotated(radians) * distance;
        if(!force && CloseOtherSites(position)) {
            return false;
        }
        Site site = (Site)Site.Instance();
        GetNode<Node2D>("Sites").AddChild(site);
        site.SetPosition(position);
        site.SetRotation(radians + (Mathf.Pi * 0.5f));
        site.SetOwner(player);
        Connect("AreaDamage", site, "onAreaDamage");
        Connect("Destroyed", site, "onAsteroidDestroyed");
        return true;
    }

    private Boolean CloseOtherSites(Vector2 position) {
        foreach(var site in GetSites()) {
            if(site.Position.DistanceTo(position) < 25) {
                return true;
            }
        }
        return false;
    }

    public List<Site> GetSites() {
        var sites = new List<Site>();
        Godot.Collections.Array site_array = GetNode<Node2D>("Sites").GetChildren();
        foreach(var site_object in site_array) {
            sites.Add((Site)site_object);
        }
        return sites;
    }

    // Called when the radius has been updated to set the sizes of the children
    private void onRadiusChanged() 
    {
        _CollisionShape.Radius = _Radius;
        // Get the scale so the final width is correct
        var base_radius = _Sprite.Texture.GetWidth() / 2;
        var scale = _Radius / base_radius;
        _Sprite.SetScale(new Vector2(scale, scale));
        foreach(var site in GetSites()) {
            var distance = _Radius + 10;
            site.Position = site.Position.Normalized() * distance;
        }
    }

    // Get the effect due to this asteroids gravity on a unit mass at the given point
    public Vector2 GetGravityEffect(Vector2 position) 
    {
        var basicForce = 1 / GlobalPosition.DistanceSquaredTo(position);
        var force = basicForce * 20 * _Radius * _Radius;
        Vector2 delta = (GlobalPosition - position).Normalized();
        return delta * force;
    }

    // Take area damage
    private void onAreaDamage(Vector2 position, float area, float damage) {
        if(GlobalPosition.DistanceTo(position) < (area + _Radius)) {
            TakeDamage(damage);
        }
        // Re-admit signal for children
        EmitSignal("AreaDamage", position, area, damage);
    }

    // The site has taken damage update radius
    public void TakeDamage(float damage) {
        float radius_damage = 0.3f * damage; // Scale to damage to radius
        _Radius -= radius_damage;
        if(_Radius > MinimumRadius) {
            // Propagate radius change
            EmitSignal("RadiusChanged");
        } else {
            Destroy();
        }
    }

    // The asteroid is destroyed
    private void Destroy() {
        // Destroy the asteroid
        EmitSignal("Destroyed", this);
        QueueFree();
    }

    // If something enteres our area iff it is a missile tell it to explode
    public void _on_Asteroid_body_entered(PhysicsBody2D body) {
        if(body is Missile) {
            var missile = (Missile)body;
            missile.onCollideExplosion();
        }
        if(body is Mine) {
            var mine = (Mine)body;
            mine.onCollideExplosion();
        }
    }
}
