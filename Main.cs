using Godot;
using System;
using System.Collections.Generic;

public class Main : Node
{

    [Export]
    public PackedScene ExplosionEffect;

    // Ordered list of players
    private List<Player> _Players;

    // Current players's turn
    private int _CurrentPlayer = 0;

    // Destroyed asteroids
    private List<Asteroid> _DestroyedAsteroids;

    // Node we are tracking the view against
    private Node2D _ViewTrack = null;

    // Signal for everything within range to take area damage
    [Signal]
    delegate void AreaDamage(Vector2 position, float area, float damage);

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
            var rand = new Random();
        // Cant seem to set this directly
        var background_tex = GetNode<Sprite>("Background").Texture;
        background_tex.Flags = 7;
        _DestroyedAsteroids = new List<Asteroid>();
        // Get list of asteroids
        var asteroids = GetAsteroids();
        foreach(var asteroid in asteroids) {
            // Set size
            asteroid.SetRadius(rand.Next(40, 90)); 
            // Set up signal handling for asteroid
            Connect("AreaDamage", asteroid, "onAreaDamage");
            asteroid.Connect("Destroyed", this, "AsteroidDestroyed");
        }
        // Get list of players
        var playerNames = new List<string> { "Player", "Player2", "Player3", "Player4"};
        var player_vars = GetNode<PlayerVars>("/root/PlayerVars");
        _Players = new List<Player>();
        for(int i = 0; i < 4; ++i) {
            var player = GetNode<Player>(playerNames[i]);
            var player_var = player_vars.Players[i];
            if(player_var.Playing) {
                player.SetDisplayName(player_var.Name);
                player.SetColour(player_var.Colour);
                _Players.Add(player);
            } else {
                // Get rid of player
                player.QueueFree();
            }
        }
        // Number sites per player
        int sites_per_player = 8 - _Players.Count;
        foreach(var player in _Players) {
            player.SetSitesPerPlayer(sites_per_player);
        }
        // Create sites
        foreach(var player in _Players) {
            for(int i=0; i < sites_per_player; ++i) {
                // Retry if couldn't place on an asteroid
                for(int retry=0; retry < 30; ++retry) {
                    // Choose asteroid
                    var index = rand.Next(0, asteroids.Count - 1);
                    Boolean force = retry == 29;
                    var success = asteroids[index].AddSite(player, rand, force);
                    if(success) {
                        break;
                    }
                }
            }
        }
        // Start the first players turn
        onPlayerAttackFinished();
    }

    // When a players attack is finished go to the next player
    private void onPlayerAttackFinished() {
        var winner = GetWinner();
        if(winner!=null) {
            var player_vars = GetNode<PlayerVars>("/root/PlayerVars");
            player_vars.Victor = new PlayerVar();
            player_vars.Victor.Name = winner.DisplayName;
            player_vars.Victor.Colour = winner.Color;
            GetTree().ChangeScene("res://WinScreen.tscn");
        }
        if(ActivePlayerCount() == 0)  {
            GetTree().ChangeScene("res://EveryoneDead.tscn");
        }
        // Get next site and start it's turn
        _CurrentPlayer++;
        if(_CurrentPlayer >= _Players.Count) {
            _CurrentPlayer = 0;
        }
        _Players[_CurrentPlayer].StartTurn();
    }

    private Player GetWinner() {
        foreach(var player in _Players) {
            if(!player.IsOut()) {
                return player;
            }
        }
        return null;
    }

    private int ActivePlayerCount() {
        int count = 0;
        foreach(var player in _Players) {
            if(!player.IsOut()) {
                count++;
            }
        }
        return count;
    }

    public void Explosion(Vector2 position) {
        // An explosion has happened here damage the sites and asteroids
        // based on distance
        EmitSignal("AreaDamage", position, 30, 24);
        // Draw a explosion
        var explosionInstance = (ExplosionEffect)ExplosionEffect.Instance();
        AddChild(explosionInstance);
        explosionInstance.Position = position;
    }

    public override void _Process(float delta) {
        if(_ViewTrack != null) {
            GetNode<Node2D>("ViewTarget").GlobalPosition = _ViewTrack.GlobalPosition;
        }
    }

    public void TargetView(Node2D tracker) {
        _ViewTrack = tracker;
    }
    public void RemoveIfTarget(Node2D tracker) {
        if(_ViewTrack == tracker) {
            _ViewTrack = null;
        }
    }

    public void MineExplosion(Vector2 position) {
        // An explosion has happened here damage the sites and asteroids
        // based on distance - minimally
        EmitSignal("AreaDamage", position, 10, 4);
        // Draw a explosion
        var explosionInstance = (ExplosionEffect)ExplosionEffect.Instance();
        AddChild(explosionInstance);
        explosionInstance.Position = position;
    }

    // An asteroid is destroed 
    private List<Asteroid> GetAsteroids() {
        var asteroids = new List<Asteroid>();
        Godot.Collections.Array asteroid_array = GetNode<Node>("Asteroids").GetChildren();
        foreach(var asteroid_object in asteroid_array) {
            asteroids.Add((Asteroid)asteroid_object);
        }
        return asteroids;
    }

    // Asteroid destyroyed mark it as dead
    private void AsteroidDestroyed(Asteroid asteroid) {
        _DestroyedAsteroids.Add(asteroid);
    }

    // Get all the sites - go through the asteroids.
    public List<Site> GetSites() {
        var sites = new List<Site>();
        foreach(var asteroid in GetAsteroids()) {
            foreach(var site in asteroid.GetSites()) {
                sites.Add(site);
            }
        }
        return sites;
    }

    public Vector2 GetGravityEffect(Vector2 position) {
        var effect = new Vector2(0,0);
        foreach(var asteroid in GetAsteroids()){ 
           effect += asteroid.GetGravityEffect(position);
        }
        return effect;
    }

    public Asteroid GetAsteroidForSite(Site site) {
        foreach(var asteroid in GetAsteroids()){ 
            if(asteroid.GetSites().Contains(site)) {
                return asteroid;
            }
        }
        return null;
    }

    public override void _UnhandledInput(InputEvent @event)
    {
        if (@event is InputEventMouseButton eventMouse) {
            if(eventMouse.IsActionPressed("zoom_in")) {
                var camera = GetNode<Camera2D>("ViewTarget/Camera2D");
                var zoom = camera.GetZoom();
                if(zoom.Length() < 3) {
                    zoom = zoom * 1.04f;
                }
                camera.SetZoom(zoom);
            }
            if(eventMouse.IsActionPressed("zoom_out")) {
                var camera = GetNode<Camera2D>("ViewTarget/Camera2D");
                var zoom = camera.GetZoom();
                if(zoom.Length() > 0.5) {
                    zoom = zoom * 0.96f;
                }
                camera.SetZoom(zoom);
            }
        }
    }

    public void DamageAsteroid(Asteroid asteroid, float damage) {
        if(asteroid != null) {
            if(!_DestroyedAsteroids.Contains(asteroid)) {
                asteroid.TakeDamage(damage);
            }
        }
    }

}
