using Godot;
using System;

public class EveryoneDead : Control
{
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GetNode<Timer>("Timer").Start();
    }

    public void Finished() 
    {
        GetTree().ChangeScene("res://StartMenu.tscn");
    }
}
