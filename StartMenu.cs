using Godot;
using System;

public class StartMenu : Control
{

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    public void onStart() {
        GetTree().ChangeScene("res://PlayerMenu.tscn");
    } 

    public void onExit() {
        GetTree().Quit();
    }
}
